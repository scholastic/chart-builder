# LOI Chart Builder

## About

Just a simple chart building interface, because nothing free and easy to use was available.


## Local setup

This project includes a Vagrant virtual machine for local development.

To spin up the Vagrant box:
1. Install Vagrant
2. `cp vagrant.example.yaml vagrant.yaml` and modify the new file to add real values
3. `vagrant up`
4. `npm i && npm run build` (either in guest VM or host machine)

That's it!


## Deployment

`npm run deploy`
