import './DatePicker.styl'
import 'flatpickr/dist/flatpickr.min.css'
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';

const propTypes = {
	inputClass: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	selectedDate: PropTypes.string.isRequired
}


const DatePicker = (props) => {
	const randomClass = Math.random().toString().slice(2);

	return (
		<div className="date-picker">
			<label>{ props.label }</label>
			<Flatpickr
				value={ moment(props.selectedDate).format('MM/DD/YYYY') }
				onChange={ (selectedDates) => props.onChange(moment(selectedDates[0]).format('YYYY-MM-DD')) }
				onReady={
					() => {
						let $input = $('input.' + randomClass);
						$input.mask('00/00/0000');
						$input.click((ev) => $input.select())

						$input.change((ev) => {
							let manualDate = moment(ev.target.value, 'MM/DD/YYYY');
							if (manualDate.isValid()) {
								props.onChange(manualDate.format('YYYY-MM-DD'));
							} else {
								return false;
							}
						});
					}
				}
				options={{
					allowInput: true,
					altFormat: "m/d/Y",
					altInput: true,
					altInputClass: props.inputClass + ' ' + randomClass,
					dateFormat: "m/d/Y",
					enableTime: false,
					static: true
				}}
				placeholder="MM/DD/YYYY"
			/>
		</div>
	);
}

DatePicker.propTypes = propTypes;
export default DatePicker;
