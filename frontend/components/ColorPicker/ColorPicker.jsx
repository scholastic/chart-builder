import PropTypes from 'prop-types'
import React, { useState } from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'
import './ColorPicker.styl'

const ColorPicker = (props) => {
	const [shouldDisplayPicker, setShouldDisplayPicker] = useState(false);

	const handleClick = () => {
		setShouldDisplayPicker(!shouldDisplayPicker);
	};

	const handleClose = () => {
		setShouldDisplayPicker(false);
	};

	const handleChange = (color) => {
		props.onChangeComplete(color.hex);
	};

	return (
		<div className="color-picker">
			<div className="swatch-container" onClick={ handleClick }>
				<div className="swatch" style={{ backgroundColor: props.color }} />
			</div>
			{
				shouldDisplayPicker ? (
					<div className="popover">
						<div className="cover" onClick={ handleClose } />
						<SketchPicker color={ props.color } onChangeComplete={ handleChange } />
					</div>
				) : null
			}
		</div>
	);
}

export default ColorPicker;
