import './Sidebar.styl';
import PropTypes from 'prop-types';
import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useCookies } from 'react-cookie';


const propTypes = {
	confirmNavigation: PropTypes.func.isRequired,
};

const Sidebar = (props) => {
	const [isCollapsed, setIsCollapsed] = useState(false);
	const [hasInitialized, setHasInitialized] = useState(false);
	const [cookies, setCookie] = useCookies(['isSidebarCollapsed', 'recentCharts']);

	useLayoutEffect(() => {
		if (hasInitialized === false) {
			if (cookies.isSidebarCollapsed === true) {
				setIsCollapsed(true);
			}

			setHasInitialized(true);
		}
	});

	useEffect(() => {
		if (isCollapsed === true) {
			setCookie('isSidebarCollapsed', true);
		} else {
			setCookie('isSidebarCollapsed', false);
		}
	}, [isCollapsed]);

	const getRecentCharts = () => {
		if (cookies.recentCharts) {
			return cookies.recentCharts;
		} else {
			return [];
		}
	}

	return (
		<div className={ 'sidebar ' + (isCollapsed ? 'collapsed' : '') }>
			<div className="sidebar-content">
				<div className="recent-charts">
					<div className="sidebar-section">
						<div className="sidebar-section-header">
							<i className="fas fa-history" />
							Recent Charts
						</div>
						<div className="sidebar-section-body">
							{
								getRecentCharts().map((recentChart) => {
									return (
										<div className="recent-chart-item" key={ "recent-chart-" + recentChart.uuid }>
											<a href="#" onClick={ (ev) => props.confirmNavigation("/" + recentChart.uuid) }>
												<div className="recent-chart-title">
													{ recentChart.title ? recentChart.title : 'Untitled chart' }
												</div>
											</a>
											<div className="recent-chart-date">
												{ recentChart.visitedAt }
											</div>
										</div>
									);
								})
							}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

Sidebar.propTypes = propTypes;
export default Sidebar;
