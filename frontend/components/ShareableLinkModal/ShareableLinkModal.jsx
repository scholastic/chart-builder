import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './ShareableLinkModal.styl'

const propTypes = {
	onHide: PropTypes.func.isRequired,
	shouldShow: PropTypes.bool,
	url: PropTypes.string.isRequired,
}

const ShareableLinkModal = (props) => {
	const [hasCopiedToClipboard, setHasCopiedToClipboard] = useState(false);

	const onHide = () => {
		setHasCopiedToClipboard(false);
		props.onHide();
	}

	const handleCopy = (ev) => {
		$(ev.target).parent().siblings('input').select();
		document.execCommand('copy');
		setHasCopiedToClipboard(true);
	}

	const handleClickUrl = (ev) => {
		$(ev.target).select();
	}

	return (
		<Modal show={ props.shouldShow } onHide={ onHide } className="shareable-link-modal">
			<Modal.Header closeButton>
				<Modal.Title>Shareable Chart Link</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<div className="input-group">
					<input
						className="form-control"
						onChange={ (ev) => ev.preventDefault() }
						onClick={ handleClickUrl }
						type="text"
						value={ props.url }
					/>
					<div className="input-group-append">
						<button
							className="btn btn-outline-secondary"
							onClick={ handleCopy }
							type="button"
						>
							<i className="fas fa-copy" />
						</button>
					</div>
				</div>
				<div className="feedback">
					{
						hasCopiedToClipboard ? (
							<span>This link has been copied to your clipboard!</span>
						) : ''
					}
				</div>
			</Modal.Body>
		</Modal>
	)
}

ShareableLinkModal.propTypes = propTypes;
export default ShareableLinkModal;
