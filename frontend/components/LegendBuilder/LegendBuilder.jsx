import './LegendBuilder.styl';
import PropTypes from 'prop-types';
import React from 'react';


const propTypes = {
	legendItems: PropTypes.arrayOf(PropTypes.shape({
		color: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired
	})),
	setLegendItems: PropTypes.func.isRequired
};

const LegendBuilder = (props) => {
	const handleChangeLegendItem = (index, label) => {
		return props.setLegendItems((previousLegendItems) => {
			return previousLegendItems.map((thisLegendItem, thisIndex) => {
				if (thisIndex === index) {
					return {
						color: thisLegendItem.color,
						label: label
					};
				} else {
					return thisLegendItem;
				}
			});
		});
	}

	return (
		<div className="legend-builder">
			{
				props.legendItems.map((legendItem, index) => {
					return (
						<div className="legend-builder-row" key={ "legend-builder-row-" + index }>
							<div className="legend-builder-color">
								<label>Color</label>
								<div className="legend-builder-swatch" style={{ backgroundColor: legendItem.color }} />
							</div>
							<div className="form-group">
								<label>Label</label>
								<input
									className="form-control"
									onChange={ (ev) => handleChangeLegendItem(index, ev.target.value) }
									type="text"
									value={ legendItem.label }
								/>
							</div>
						</div>
					)
				})
			}
		</div>
	);
}

LegendBuilder.propTypes = propTypes;
export default LegendBuilder;
