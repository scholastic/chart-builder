import './Header.styl';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import uuidv4 from 'uuid/v4';
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';


const propTypes = {
	confirmNavigation: PropTypes.func.isRequired,
}

const Header = (props) => {
	return (
		<div className="header">
			<Link to="/">
				<img src="/images/loi-logo-horizontal.png" />
			</Link>

			<button
				className="btn btn-outline-primary new-chart-button"
				onClick={ () => props.confirmNavigation('/' + uuidv4()) }
				type="button"
			>
				<i className="fas fa-plus" />
				{' '}
				New chart
			</button>
		</div>
	)
}

Header.propTypes = propTypes;
export default Header;
