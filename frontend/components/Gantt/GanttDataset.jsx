import ColorPicker from '../ColorPicker/ColorPicker';
import DatePicker from '../DatePicker/DatePicker';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

const propTypes = {
	dataset: PropTypes.object.isRequired,
	defaultColor: PropTypes.string.isRequired,
	isFirstDataset: PropTypes.bool,
	isLastDataset: PropTypes.bool,
	moveDatasetDown: PropTypes.func.isRequired,
	moveDatasetUp: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired
}

const GanttDataset = (props) => {
	useEffect(() => {
		if (moment(props.dataset.startDate).isSameOrAfter(props.dataset.endDate)) {
			props.onChange('endDate', moment(props.dataset.startDate).add('1', 'day').format('YYYY-MM-DD'));
		}
	}, [props.dataset.startDate]);

	useEffect(() => {
		if (moment(props.dataset.startDate).isSameOrAfter(props.dataset.endDate)) {
			props.onChange('startDate', moment(props.dataset.endDate).subtract('1', 'day').format('YYYY-MM-DD'));
		}
	}, [props.dataset.endDate]);

	return (
		<div className="dataset-form">
			<div className="dataset-order-controls">
				<i
					className={ "fas fa-angle-up" + (props.isFirstDataset ? ' disabled' : '') }
					onClick={ () => props.moveDatasetUp() }
				/>
				<i
					className={ "fas fa-angle-down" + (props.isLastDataset ? ' disabled' : '') }
					onClick={ () => props.moveDatasetDown() }
				/>
			</div>
			<div className="form-group">
				<label>Label</label>
				<input
					className="form-control"
					onChange={ (ev) => props.onChange('label', ev.target.value) }
					type="text"
					value={ props.dataset.label }
				/>
			</div>
			<DatePicker
				inputClass="form-control start-date"
				label="Start date"
				onChange={ (date) => props.onChange('startDate', date) }
				selectedDate={ props.dataset.startDate }
			/>
			<DatePicker
				inputClass="form-control end-date"
				label="End date"
				onChange={ (date) => props.onChange('endDate', date) }
				selectedDate={ props.dataset.endDate }
			/>
			<div className="dataset-color">
				<label>Color</label>
				<ColorPicker
					color={ props.dataset.color === null ? props.defaultColor : props.dataset.color }
					onChangeComplete={ (color) => props.onChange('color', color) }
				/>
			</div>
			<button
				className="btn btn-outline-danger remove-dataset"
				onClick={ props.onDelete }
				type="button"
			>
				<i className="fa fa-trash-alt" />
			</button>
		</div>
	);
}

GanttDataset.propTypes = propTypes;
export default GanttDataset;
