import './GanttBuilder.styl';
import _ from 'lodash';
import chartColors from '../../libraries/chartColors';
import ConfirmationModal from '../ConfirmationModal/ConfirmationModal';
import fetchJson from '../../libraries/fetchJson';
import GanttChart from './GanttChart';
import GanttDataset from './GanttDataset';
import GanttSettings from './GanttSettings';
import Legend from '../Legend/Legend';
import LegendBuilder from '../LegendBuilder/LegendBuilder';
import LoadingModal from '../LoadingModal/LoadingModal';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import ShareableLinkModal from '../ShareableLinkModal/ShareableLinkModal';
import uuidv4 from 'uuid/v4';
import { Modal } from 'react-bootstrap';
import { useCookies } from 'react-cookie';

const propTypes = {
	flash: PropTypes.func.isRequired,
	hasUnsavedChanges: PropTypes.bool,
	history: PropTypes.object.isRequired,
	match: PropTypes.object.isRequired,
	setHasUnsavedChanges: PropTypes.func.isRequired
};

const GanttBuilder = (props) => {
	// Initial values are defined to make it easier to reset state when a new chart is created
	const initialChartEndDate = moment().endOf('month').startOf('week').format('YYYY-MM-DD');
	const initialChartHeight = '600';
	const initialChartStartDate = moment().startOf('month').startOf('week').format('YYYY-MM-DD');
	const initialChartTitle = '';
	const initialChartWidth = '800';
	const initialDefaultColor = chartColors.blue;
	const initialLineThickness = '15';
	const initialTimeInterval = 'week';
	const emptyDataset = {
		label: '',
		startDate: moment().format('YYYY-MM-DD'),
		endDate: moment().add('1', 'day').format('YYYY-MM-DD'),
		color: null
	};
	const initialDatasets = [emptyDataset];

	const [chartEndDate, setChartEndDate] = useState(initialChartEndDate);
	const [chartHeight, setChartHeight] = useState(initialChartHeight);
	const [chartStartDate, setChartStartDate] = useState(initialChartStartDate);
	const [chartTitle, setChartTitle] = useState(initialChartTitle);
	const [chartWidth, setChartWidth] = useState(initialChartWidth);
	const [defaultColor, setDefaultColor] = useState(initialDefaultColor);
	const [hasRestoredState, setHasRestoredState] = useState(false);
	const [indexBeingDeleted, setIndexBeingDeleted] = useState(null);
	const [isEditingChartSettings, setIsEditingChartSettings] = useState(false);
	const [isEditingDatasets, setIsEditingDatasets] = useState(true);
	const [isGettingLink, setIsGettingLink] = useState(false);
	const [lineThickness, setLineThickness] = useState(initialLineThickness);
	const [timeInterval, setTimeInterval] = useState(initialTimeInterval);
	const [datasets, setDatasets] = useState(initialDatasets);

	const resetState = () => {
		setChartEndDate(initialChartEndDate);
		setChartHeight(initialChartHeight);
		setChartStartDate(initialChartStartDate);
		setChartTitle(initialChartTitle);
		setChartWidth(initialChartWidth);
		setDefaultColor(initialDefaultColor);
		setHasRestoredState(false);
		setIndexBeingDeleted(null);
		setIsEditingChartSettings(false);
		setIsGettingLink(false);
		setLineThickness(initialLineThickness);
		setTimeInterval(initialTimeInterval);
		setDatasets(initialDatasets);
	}

	/**
	 * RECENT CHART HISTORY MANAGEMENT
	 */
	const [cookies, setCookie] = useCookies(['recentCharts']);
	const [shouldRecordChartHistory, setShouldRecordChartHistory] = useState(false);

	const recordChartHistory = (shouldSaveBlankTitle = false) => {
		const uuid = props.match.params.uuid;

		if (uuid && (chartTitle || shouldSaveBlankTitle)) {
			// Set a cookie containing the most recent charts visited, in descending order by date
			let chartHistoryEntry = {
				uuid: uuid,
				title: chartTitle,
				visitedAt: moment().format('MM/DD/YYYY h:mm:ssa')
			};

			let recentCharts;

			if (cookies.recentCharts) {
				// Remove existing entries for this chart
				recentCharts = cookies.recentCharts.filter((recentChart) => recentChart.uuid !== uuid);
				// Then add this chart to the beginning of the array
				recentCharts.unshift(chartHistoryEntry);
			} else {
				recentCharts = [chartHistoryEntry];
			}

			if (recentCharts.length > 10) {
				recentCharts = recentCharts.filter((chart, index) => index <= 9);
			}

			setCookie('recentCharts', recentCharts);
		}
	};

	// Update the history any time the chart title changes
	// This will also serve to record a chart in the history any time it's successfully restored from the server
	useEffect(() => {
		if (shouldRecordChartHistory === true) {
			recordChartHistory();
			setShouldRecordChartHistory(false);
		}
	}, [chartTitle, shouldRecordChartHistory]);


	/**
	 * RESTORING STATE
	 */
	const [isRestoringState, setIsRestoringState] = useState(false);
	const [shouldRestoreState, setShouldRestoreState] = useState(false);

	// On component mount, if a UUID is present on the URL, restore the chart from the DB.
	useEffect(() => {
		if (!hasRestoredState && !shouldRestoreState && !isRestoringState) {
			const uuid = props.match.params.uuid;

			if (uuid) {
				setShouldRestoreState(true);
			} else {
				props.history.push(uuidv4());
			}
		}
	});

	// Any time the UUID on the URL changes, restore the chart
	useEffect(() => {
		if (props.match.params.uuid) {
			setHasRestoredState(false);
			setShouldRestoreState(true);
		}
	}, [props.match.params.uuid]);

	useEffect(() => {
		const uuid = props.match.params.uuid;

		if (shouldRestoreState === true && uuid) {
			const url = '/api/charts/' + uuid;
			setIsRestoringState(true);

			fetchJson(url).then((response) => {
				if (response && response.success && response.payload) {
					const chartState = JSON.parse(response.payload);

					setChartEndDate(chartState.chartEndDate);
					setChartHeight(chartState.chartHeight);
					setChartStartDate(chartState.chartStartDate);
					setChartTitle(chartState.chartTitle);
					setChartWidth(chartState.chartWidth);
					setDatasets(chartState.datasets);
					setDefaultColor(chartState.defaultColor);
					setIsDisplayingLegend(chartState.isDisplayingLegend);
					setLineThickness(chartState.lineThickness);
					setTimeInterval(chartState.timeInterval);

					// Charts created before the legend was implemented will not have legend items stored
					// in the DB, so they need to be defined from scratch
					if (chartState.legendItems) {
						setLegendItems(chartState.legendItems);
					} else {
						defineLegendItems();
					}

					// When restoring state, collapse the accordions by default
					setIsEditingChartSettings(false);
					setIsEditingDatasets(false);

					setHasRestoredState(true);
					setShouldRestoreState(false);
					setIsRestoringState(false);
					setShouldRecordChartHistory(true);
				}
			}).catch(() => {
				resetState();
				setHasRestoredState(true);
				setShouldRestoreState(false);
				setIsRestoringState(false);
			});
		}
	}, [shouldRestoreState]);


	/**
	 * SAVING STATE
	 */
	const [isSavingState, setIsSavingState] = useState(false);
	const [shouldSaveState, setShouldSaveState] = useState(false);

	useEffect(() => {
		if (shouldSaveState === true) {
			const serializedState = JSON.stringify({
				chartEndDate,
				chartHeight,
				chartStartDate,
				chartTitle,
				chartWidth,
				datasets,
				defaultColor,
				isDisplayingLegend,
				legendItems,
				lineThickness,
				timeInterval
			});
			const uuid = props.match.params.uuid;
			const url = '/api/charts/' + uuid;
			const fetchOptions = {
				method: 'PATCH',
				body: {
					uuid: uuid,
					state: serializedState
				}
			};

			setIsSavingState(true);

			fetchJson(url, fetchOptions).then(() => {
				setShouldSaveState(false);
				setIsSavingState(false);
				props.setHasUnsavedChanges(false);
				props.flash('Chart saved!', 'success');
			}).catch(() => {
				setShouldSaveState(false);
				setIsSavingState(false);
				props.flash('Your chart could not be saved', 'error');
			});

			const shouldSaveBlankTitle = true;
			recordChartHistory(shouldSaveBlankTitle);
		}
	}, [shouldSaveState]);


	/**
	 * DATASET MANAGEMENT
	 */
	const handleChangeDataset = (index, key, value) => {
		return setDatasets((previousDatasets) => {
			return previousDatasets.map((dataset, thisIndex) => {
				if (thisIndex === index) {
					let additions = {};
					additions[key] = value;
					return Object.assign({}, dataset, additions);
				} else {
					return dataset;
				}
			});
		});
	}

	const addDataset = () => {
		return setDatasets((previousDatasets) => {
			return datasets.concat([emptyDataset]);
		});
	}

	const removeDataset = (index) => {
		if (datasets.length === 1) {
			return setDatasets([emptyDataset]);
		} else {
			return setDatasets((previousDatasets) => {
				return datasets.filter((dataset, thisIndex) => thisIndex !== index);
			});
		}
	}

	const confirmDeletion = () => {
		setIndexBeingDeleted(null);
		removeDataset(indexBeingDeleted);
	}

	const moveDatasetUp = (index) => {
		if (index === 0) {
			return false;
		} else {
			return setDatasets((datasets) => {
				return [
					...datasets.slice(0, index - 1),
					datasets[index],
					datasets[index - 1],
					...datasets.slice(index + 1)
				]
			});
		}
	}

	const moveDatasetDown = (index) => {
		if (index === datasets.length - 1) {
			return false;
		} else {
			return setDatasets((datasets) => {
				return [
					...datasets.slice(0, index),
					datasets[index + 1],
					datasets[index],
					...datasets.slice(index + 2)
				]
			});
		}
	}

	const sortDatasetsByStartDate = () => {
		return setDatasets((datasets) => {
			return _.sortBy(datasets, ['startDate', 'endDate']);
		});
	}

	const sortDatasetsByEndDate = () => {
		return setDatasets((datasets) => {
			return _.sortBy(datasets, ['endDate', 'startDate']);
		});
	}


	/**
	 * CHART CONTROLS
	 */
	const handleClickShareableLink = () => {
		setShouldSaveState(true);
		setIsGettingLink(true);
	}

	const getSaveButtonIconClass = () => {
		if (isSavingState) {
			return "fas fa-cog fa-spin";
		} else if (props.hasUnsavedChanges) {
			return "fas fa-save";
		} else {
			return "fas fa-check";
		}
	}


	/**
	 * LEGEND
	 */
	const [legendItems, setLegendItems] = useState([]);
	const [isDisplayingLegend, setIsDisplayingLegend] = useState(true);
	const [isEditingLegend, setIsEditingLegend] = useState(false);

	const defineLegendItems = () => {
		if (datasets && datasets.length) {
			let newLegendItems = [];

			// Get a unique array of the colors in use in the chart
			let colors = datasets.map((dataset) => dataset.color ? dataset.color : defaultColor);
			colors = colors.filter((color, index) => colors.indexOf(color) === index);

			// Loop through the colors in use.  If legend items already exist for this color, keep it
			// Otherwise, define a new legend item with a blank label
			colors.map((color) => {
				const existingLegendItems = legendItems.filter((legendItem) => legendItem.color === color);

				if (existingLegendItems.length === 0) {
					newLegendItems.push({ color: color, label: '' });
				} else {
					newLegendItems.push(existingLegendItems[0]);
				}
			});

			setLegendItems(newLegendItems);
		} else {
			setLegendItems([]);
		}
	}

	// Any time a dataset is changed, define a set of `legendItems` for each color in use on the chart
	useEffect(defineLegendItems, [datasets]);

	/**
	 * UNSAVED CHANGE TRACKING
	 */

	// Whenever the chart is changed, set `hasUnsavedChanges` to true
	useEffect(() => {
		if (hasRestoredState) {
			props.setHasUnsavedChanges(true);
		}
	}, [
		chartEndDate,
		chartHeight,
		chartStartDate,
		chartTitle,
		chartWidth,
		defaultColor,
		isDisplayingLegend,
		legendItems,
		lineThickness,
		timeInterval,
		datasets
	]);

	/**
	 * RENDER
	 */
	return (
		<div className="gantt-builder chart-builder">
			<div className="chart-container" style={{ height: chartHeight + 'px', width: chartWidth + 'px' }}>
				<GanttChart
					chartStartDate={ chartStartDate }
					chartEndDate={ chartEndDate }
					datasets={ datasets }
					defaultColor={ defaultColor }
					lineThickness={ lineThickness }
					timeInterval={ timeInterval }
					title={ chartTitle }
				/>
			</div>

			{
				isDisplayingLegend ? (
					<div className="legend-container">
						<Legend legendItems={ legendItems } />
					</div>
				) : ''
			}

			<div className="chart-controls">
				<button
					className="btn btn-outline-secondary"
					onClick={ () => setIsEditingChartSettings(true) }
					type="button"
				>
					<i className="fas fa-cog" />
					{' '}
					Edit chart settings
				</button>

				<button
					className="get-chart-link btn btn-outline-primary"
					disabled={ isSavingState || isRestoringState }
					onClick={ handleClickShareableLink }
					type="button"
				>
					<i className="fas fa-share" />
					{' '}
					Get shareable link
				</button>

				<button
					className={ "save-chart btn " + (props.hasUnsavedChanges ? "btn-success" : "btn-outline-success") }
					disabled={ isSavingState || isRestoringState || !props.hasUnsavedChanges }
					onClick={ () => setShouldSaveState(true) }
					type="button"
				>
					<i className={ getSaveButtonIconClass() } />
					{' '}
					{
						props.hasUnsavedChanges ? 'Save changes' : 'Up to date'
					}
				</button>
			</div>

			{
				isDisplayingLegend ? (
					<div className="legend-forms accordion-card card">
						<div
							className={ "card-header toggle-collapse" + (isEditingLegend ? '' : ' collapsed') }
							onClick={ () => setIsEditingLegend(!isEditingLegend) }
						>
							<i className={ "fas fa-caret-" + (isEditingLegend ? "down" : "right") } />
							Legend
						</div>
						{
							isEditingLegend ? (
								<div className="card-body">
									<LegendBuilder legendItems={ legendItems } setLegendItems={ setLegendItems } />
								</div>
							) : ''
						}
					</div>
				) : ''
			}

			<div className="dataset-forms accordion-card card">
				<div
					className={ "card-header toggle-collapse" + (isEditingDatasets ? '' : ' collapsed') }
					onClick={ () => setIsEditingDatasets(!isEditingDatasets) }
				>
					<i className={ "fas fa-caret-" + (isEditingDatasets ? "down" : "right") } />
					Datasets
				</div>
				{
					isEditingDatasets ? (
						<div className="card-body">
							<div className="dataset-forms-controls">
								<button
									className="btn btn-outline-secondary btn-icon-left"
									onClick={ sortDatasetsByStartDate }
									type="button"
								>
									<i className="fas fa-sort-amount-down" />
									Sort by start date
								</button>

								<button
									className="btn btn-outline-secondary btn-icon-left"
									onClick={ sortDatasetsByEndDate }
									type="button"
								>
									<i className="fas fa-sort-amount-down" />
									Sort by end date
								</button>
							</div>
							{
								datasets.map((dataset, index) => {
									return (
										<GanttDataset
											dataset={ datasets[index] }
											defaultColor={ defaultColor }
											isFirstDataset={ index === 0 }
											isLastDataset={ index === datasets.length - 1 }
											key={ "dataset-form-" + index }
											moveDatasetDown={ () => moveDatasetDown(index) }
											moveDatasetUp={ () => moveDatasetUp(index) }
											onChange={ (key, value) => handleChangeDataset(index, key, value) }
											onDelete={ () => dataset.label === '' ? removeDataset(index) : setIndexBeingDeleted(index) }
										/>
									)
								})
							}
							<button
								className="btn btn-info add-dataset"
								onClick={ addDataset }
								type="button"
							>
								<i className="fa fa-plus" />
							</button>
						</div>
					) : ''
				}
			</div>

			<ConfirmationModal
				cancelButtonClass="btn btn-outline-secondary"
				confirmButtonClass="btn btn-danger"
				message={ 'Are you sure you want to delete item ' + (indexBeingDeleted === null ? '' : datasets[indexBeingDeleted].label) + '?' }
				onCancel={ () => setIndexBeingDeleted(null) }
				onConfirm={ confirmDeletion }
				onHide={ () => setIndexBeingDeleted(null) }
				shouldShow={ indexBeingDeleted !== null }
				title="Are you sure?"
			/>

			<LoadingModal message="Loading chart" shouldShow={ isRestoringState } />

			<ShareableLinkModal
				onHide={ () => setIsGettingLink(false) }
				shouldShow={ isGettingLink }
				url={ window.location.href }
			/>

			<GanttSettings
				chartEndDate={ chartEndDate }
				chartHeight={ chartHeight }
				chartStartDate={ chartStartDate }
				chartTitle={ chartTitle }
				chartWidth={ chartWidth }
				defaultColor={ defaultColor }
				isDisplayingLegend={ isDisplayingLegend }
				lineThickness={ lineThickness }
				onHide={ () => setIsEditingChartSettings(false) }
				setChartEndDate={ setChartEndDate }
				setChartHeight={ setChartHeight }
				setChartStartDate={ setChartStartDate }
				setChartTitle={ setChartTitle }
				setChartWidth={ setChartWidth }
				setDefaultColor={ setDefaultColor }
				setIsDisplayingLegend={ setIsDisplayingLegend }
				setLineThickness={ setLineThickness }
				setTimeInterval={ setTimeInterval }
				shouldShow={ isEditingChartSettings }
				timeInterval={ timeInterval }
			/>
		</div>
	);
}

GanttBuilder.propTypes = propTypes;
export default GanttBuilder;
