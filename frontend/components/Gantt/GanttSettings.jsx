import chartColors from '../../libraries/chartColors'
import ColorPicker from '../ColorPicker/ColorPicker'
import DatePicker from '../DatePicker/DatePicker';
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';


const propTypes = {
	chartEndDate: PropTypes.string.isRequired,
	chartHeight: PropTypes.string.isRequired,
	chartStartDate: PropTypes.string.isRequired,
	chartTitle: PropTypes.string.isRequired,
	chartWidth: PropTypes.string.isRequired,
	defaultColor: PropTypes.string.isRequired,
	isDisplayingLegend: PropTypes.bool,
	lineThickness: PropTypes.string.isRequired,
	onHide: PropTypes.func.isRequired,
	setChartEndDate: PropTypes.func.isRequired,
	setChartHeight: PropTypes.func.isRequired,
	setChartStartDate: PropTypes.func.isRequired,
	setChartTitle: PropTypes.func.isRequired,
	setChartWidth: PropTypes.func.isRequired,
	setDefaultColor: PropTypes.func.isRequired,
	setIsDisplayingLegend: PropTypes.func.isRequired,
	setLineThickness: PropTypes.func.isRequired,
	setTimeInterval: PropTypes.func.isRequired,
	shouldShow: PropTypes.bool,
	timeInterval: PropTypes.string.isRequired,
}

const GanttSettings = (props) => {
	const [chartHeightInputValue, setChartHeightInputValue] = useState(props.chartHeight);
	const [chartWidthInputValue, setChartWidthInputValue] = useState(props.chartWidth);
	const [chartEndDateInputValue, setChartEndDateInputValue] = useState(props.chartEndDate);
	const [chartStartDateInputValue, setChartStartDateInputValue] = useState(props.chartStartDate);
	const [chartTitleInputValue, setChartTitleInputValue] = useState(props.chartTitle);
	const [defaultColorInputValue, setDefaultColorInputValue] = useState(props.defaultColor);
	const [isDisplayingLegendInputValue, setIsDisplayingLegendInputValue] = useState(props.isDisplayingLegend);
	const [timeIntervalInputValue, setTimeIntervalInputValue] = useState(props.timeInterval);
	const [lineThicknessInputValue, setLineThicknessInputValue] = useState(props.lineThickness);

	const applyChartSettings = () => {
		props.setChartEndDate(chartEndDateInputValue);
		props.setChartHeight(chartHeightInputValue);
		props.setChartStartDate(chartStartDateInputValue);
		props.setChartTitle(chartTitleInputValue);
		props.setChartWidth(chartWidthInputValue);
		props.setDefaultColor(defaultColorInputValue);
		props.setIsDisplayingLegend(isDisplayingLegendInputValue);
		props.setLineThickness(lineThicknessInputValue);
		props.setTimeInterval(timeIntervalInputValue);
		props.onHide();
	}

	const populateFormWithProps = () => {
		props.onHide();
		setChartEndDateInputValue(props.chartEndDate);
		setChartHeightInputValue(props.chartHeight);
		setChartStartDateInputValue(props.chartStartDate);
		setChartTitleInputValue(props.chartTitle);
		setChartWidthInputValue(props.chartWidth);
		setDefaultColorInputValue(props.defaultColor);
		setIsDisplayingLegendInputValue(props.isDisplayingLegend);
		setLineThicknessInputValue(props.lineThickness);
		setTimeIntervalInputValue(props.timeInterval);
	}

	// If settings props change, update the form
	useEffect(() => {
		populateFormWithProps();
	}, [
		props.chartEndDate,
		props.chartHeight,
		props.chartStartDate,
		props.chartTitle,
		props.chartWidth,
		props.defaultColor,
		props.isDisplayingLegend,
		props.lineThickness,
		props.timeInterval
	]);

	const shouldDisableChartSettingsControls = (
		props.chartEndDate === chartEndDateInputValue &&
		props.chartHeight === chartHeightInputValue &&
		props.chartStartDate === chartStartDateInputValue &&
		props.chartTitle === chartTitleInputValue &&
		props.chartWidth === chartWidthInputValue &&
		props.defaultColor === defaultColorInputValue &&
		props.isDisplayingLegend === isDisplayingLegendInputValue &&
		props.lineThickness === lineThicknessInputValue &&
		props.timeInterval === timeIntervalInputValue
	);

	return (
		<Modal
			size="lg"
			className="gantt-settings"
			show={ props.shouldShow }
		>
			<Modal.Header>
				<h3>Chart Settings</h3>
			</Modal.Header>
			<Modal.Body>
				<div className="chart-settings-row row">
					<div className="col-md-12">
						<div className="form-group">
							<label>Chart Title</label>
							<input
								className="form-control"
								type="text"
								onChange={ (ev) => setChartTitleInputValue(ev.target.value) }
								value={ chartTitleInputValue }
							/>
						</div>
					</div>
				</div>

				<h4>Dates</h4>
				<div className="chart-settings-row row">
					<div className="col-md-4">
						<DatePicker
							inputClass="chart-start-date form-control"
							label="Chart start date"
							onChange={ (date) => setChartStartDateInputValue(date) }
							selectedDate={ chartStartDateInputValue }
						/>
					</div>
					<div className="col-md-4">
						<DatePicker
							inputClass="chart-end-date form-control"
							label="Chart end date"
							onChange={ (date) => setChartEndDateInputValue(date) }
							selectedDate={ chartEndDateInputValue }
						/>
					</div>
					<div className="col-md-4">
						<div className="form-group">
							<label>Date axis time interval</label>
							<select
								className="form-control"
								onChange={ (ev) => setTimeIntervalInputValue(ev.target.value) }
								value={ timeIntervalInputValue }
							>
								<option value="day">Day</option>
								<option value="week">Week</option>
								<option value="month">Month</option>
								<option value="year">Year</option>
							</select>
						</div>
					</div>
				</div>

				<h4>Dimensions</h4>
				<div className="chart-settings-row row">
					<div className="col-md-4">
						<div className="form-group">
							<label>Chart width (px)</label>
							<input
								className="form-control"
								onChange={ (ev) => setChartWidthInputValue(ev.target.value) }
								type="text"
								value={ chartWidthInputValue }
							/>
						</div>
					</div>

					<div className="col-md-4">
						<div className="form-group">
							<label>Chart height (px)</label>
							<input
								className="form-control"
								onChange={ (ev) => setChartHeightInputValue(ev.target.value) }
								type="text"
								value={ chartHeightInputValue }
							/>
						</div>
					</div>
					<div className="col-md-4">
						<div className="form-group">
							<label>Line thickness (px)</label>
							<input
								className="form-control"
								onChange={ (ev) => setLineThicknessInputValue(ev.target.value) }
								type="text"
								value={ lineThicknessInputValue }
							/>
						</div>
					</div>
				</div>

				<h4>Appearance</h4>
				<div className="chart-settings-row row">
					<div className="col-md-4">
						<label>Default color</label>
						<ColorPicker
							color={ defaultColorInputValue }
							onChangeComplete={ (color) => setDefaultColorInputValue(color) }
						/>
					</div>

					<div className="col-md-4">
						<div>
							<label>Display legend?</label>
						</div>
						<div className="btn-group">
							<button
								className={ "btn " + (isDisplayingLegendInputValue ? 'btn-success active' : 'btn-outline-secondary') }
								onClick={ () => setIsDisplayingLegendInputValue(true) }
								type="button"
							>
								Yes
							</button>
							<button
								className={ "btn " + (isDisplayingLegendInputValue ? 'btn-outline-secondary' : 'btn-danger active') }
								onClick={ () => setIsDisplayingLegendInputValue(false) }
								type="button"
							>
								No
							</button>
						</div>
					</div>
				</div>
			</Modal.Body>
			<Modal.Footer>
				<div className="chart-settings-controls">
					<button
						className="btn btn-outline-dark"
						onClick={ populateFormWithProps }
						type="button"
					>
						Cancel
					</button>
					{' '}
					<button
						className="btn btn-primary"
						disabled={ shouldDisableChartSettingsControls }
						onClick={ applyChartSettings }
						type="button"
					>
						Apply
					</button>
				</div>
			</Modal.Footer>
		</Modal>
	)
}

GanttSettings.propTypes = propTypes;

export default GanttSettings;
