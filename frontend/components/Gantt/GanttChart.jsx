import React, { useState } from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import PropTypes from 'prop-types';

const propTypes = {
	chartStartDate: PropTypes.string.isRequired,
	chartEndDate: PropTypes.string.isRequired,
	datasets: PropTypes.array.isRequired,
	defaultColor: PropTypes.string.isRequired,
	lineThickness: PropTypes.string.isRequired,
	timeInterval: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired
}

const GanttChart = (props) => {
	const getChartData = () => {
		// Empty datasets with blank labels are provided before and after the real data in order to avoid having datasets
		// appear at the extreme top and bottom of the chart

		let chartData = {
			yLabels: [' ', ...props.datasets.map((dataset) => dataset.label), '  '],
			datasets: [{
				label: ' ',
				fill: false,
				backgroundColor: '#FFF',
				borderColor: '#FFF',
				borderWidth: 0,
				pointRadius: 0,
				data: []
			}]
		};

		props.datasets.map((dataset, index) => {
			const color = dataset.color === null ? props.defaultColor : dataset.color;

			chartData.datasets.push({
				label: dataset.label,
				fill: false,
				backgroundColor: color,
				borderColor: color,
				borderWidth: props.lineThickness,
				pointRadius: 0,
				data: [
					{ x: dataset.startDate, y: dataset.label },
					{ x: dataset.endDate, y: dataset.label }
				]
			});
		});

		chartData.datasets.push({
			label: ' ',
			fill: false,
			backgroundColor: '#FFF',
			borderColor: '#FFF',
			borderWidth: 0,
			pointRadius: 0,
			data: []
		});

		return chartData;
	}

	const getChartOptions = () => {
		return {
			animation: false,
			hover: {
				mode: 'nearest',
				intersect: true
			},
			layout: {
				padding: {
					right: 25
				}
			},
			legend: {
				display: false
			},
			maintainAspectRatio: false,
			responsive: true,
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Date'
					},
					type: 'time',
					time: {
						unit: props.timeInterval,
						min: props.chartStartDate,
						max: props.chartEndDate
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Project'
					},
					ticks: {
						beginAtZero: true,
						stepSize: 1
					},
					type: 'category'
				}],
			},
			spanGaps: true,
			title: {
				display: true,
				fontSize: 24,
				text: props.title
			},
			tooltips: {
				bodyFontFamily: '"Lucida Console", Monaco, monospace',
				mode: 'nearest',
				intersect: false,
				custom: (tooltip) => {
					if (!tooltip) return;
					// disable displaying the color box
					tooltip.displayColors = false;
				},
				callbacks: {
					label: (tooltipItem, data) => {
						const dataPoints = data.datasets[tooltipItem.datasetIndex].data;
						return [
							'Start: ' + moment(dataPoints[0].x).format('MM/DD/YYYY'),
							'End:   ' + moment(dataPoints[1].x).format('MM/DD/YYYY')
						];
					},
					title: (tooltipItem, data) => {
						return tooltipItem[0].yLabel;
					}
				}
			},
		}
	}

	return (
		<Line
			data={ getChartData() }
			datasetKeyProvider={ () => Math.random() + '-' + Math.random() }
			options={ getChartOptions() }
		/>
	)
}

GanttChart.propTypes = propTypes;
export default GanttChart;
