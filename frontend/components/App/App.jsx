import './App.styl'
import NavigationConfirmationModal from '../NavigationConfirmationModal/NavigationConfirmationModal';
import Flash from '../Flash/Flash';
import Footer from '../Footer/Footer';
import GanttBuilder from '../Gantt/GanttBuilder';
import Header from '../Header/Header';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Sidebar from '../Sidebar/Sidebar';
import { BrowserRouter as Router, Route, Link, withRouter } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';


export default function App() {
	/**
	 * FLASH
	 */
	const [flashMessage, setFlashMessage] = useState(null);
	const [shouldShowFlash, setShouldShowFlash] = useState(false);
	const [flashType, setFlashType] = useState('success');

	const flash = (message, type = 'success') => {
		setFlashMessage(message);
		setFlashType(type);
		setShouldShowFlash(true);
	};

	useEffect(() => {
		if (shouldShowFlash === true) {
			setTimeout(() => {
				setShouldShowFlash(false);
			}, 1500);
		}
	}, [shouldShowFlash]);

	/**
	 * NAVIGATION CONFIRMATION
	 */
	const [hasUnsavedChanges, setHasUnsavedChanges] = useState(false);
	const [isConfirmingNavigation, setIsConfirmingNavigation] = useState(false);
	const [navigationTarget, setNavigationTarget] = useState(null);

	const confirmNavigation = (target) => {
		// There's a race condition here, but it's highly unlikely that the user will click Confirm before both
		// of these operations have completed
		setNavigationTarget(target);
		setIsConfirmingNavigation(true);
	}

	const renderGanttBuilder = (routeProps) => {
		return (
			<GanttBuilder
				{...routeProps}
				confirmNavigation={ confirmNavigation }
				flash={ flash }
				hasUnsavedChanges={ hasUnsavedChanges }
				setHasUnsavedChanges={ setHasUnsavedChanges }
			/>
		)
	}

	return (
		<Router>
			<CookiesProvider>
				<Header confirmNavigation={ confirmNavigation } />
				<Sidebar confirmNavigation={ confirmNavigation } />
				<div className="app-content">
					<Route exact path="/" render={ renderGanttBuilder } />
					<Route exact path="/:uuid" render={ renderGanttBuilder } />
				</div>
				<Footer />

				<Flash message={ flashMessage } shouldShow={ shouldShowFlash } type={ flashType } />

				<NavigationConfirmationModal
					hasUnsavedChanges={ hasUnsavedChanges }
					hide={ () => setIsConfirmingNavigation(false) }
					onCancel={ () => setIsConfirmingNavigation(false) }
					setHasUnsavedChanges={ setHasUnsavedChanges }
					shouldShow={ isConfirmingNavigation }
					target={ navigationTarget }
				/>
			</CookiesProvider>
		</Router>
	);
}
