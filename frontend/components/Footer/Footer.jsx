import './Footer.styl';
import moment from 'moment';
import React from 'react';

export default function Footer() {
	return (
		<div className="footer">
			<div className="footer-text">
				Copyright © { new Date().getFullYear() } Learning Ovations Inc. All rights reserved.
			</div>
		</div>
	)
};
