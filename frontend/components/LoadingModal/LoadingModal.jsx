import './LoadingModal.styl';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import React from 'react';
import { Button, Modal } from 'react-bootstrap';

const propTypes = {
	message: PropTypes.string.isRequired,
	shouldShow: PropTypes.bool
};

const LoadingModal = (props) => {
	return (
		<Modal className="loading-modal" show={ props.shouldShow }>
			<Modal.Body>
				<LoadingSpinner />
				<div className="modal-title">
					{ props.message }
				</div>
			</Modal.Body>
		</Modal>
	);
}

LoadingModal.propTypes = propTypes;
export default LoadingModal;
