import PropTypes from 'prop-types';
import React from 'react';

const propTypes = {
	spin: PropTypes.bool
}

const LoadingSpinner = (props) => {
	let className = 'fa fa-cog';

	// Spin by default.
	if (props.spin !== false) {
		className += ' fa-spin';
	}

	return (<i className={className} />);
}

LoadingSpinner.propTypes = propTypes;
export default LoadingSpinner;
