import ConfirmationModal from '../ConfirmationModal/ConfirmationModal';
import PropTypes from 'prop-types';
import React, { useLayoutEffect } from 'react';
import { withRouter } from 'react-router-dom';

const propTypes = {
	hasUnsavedChanges: PropTypes.bool,
	hide: PropTypes.func.isRequired,
	history: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired,
	match: PropTypes.object.isRequired,
	onCancel: PropTypes.func.isRequired,
	setHasUnsavedChanges: PropTypes.func.isRequired,
	shouldShow: PropTypes.bool,
	target: PropTypes.string
};

const defaultProps = {
	shouldShow: false,
	target: '#'
}

const NavigationConfirmationModal = (props) => {
	// If the modal is flipped on but there are no unsaved changes, just navigate immediately to the target
	// and hide the modal
	useLayoutEffect(() => {
		if (props.shouldShow && !props.hasUnsavedChanges) {
			props.hide();
			props.history.push(props.target);
		}
	}, [props.shouldShow]);

	const handleConfirmNavigation = () => {
		props.setHasUnsavedChanges(false);
		props.history.push(props.target);
		props.hide();
	}

	return (
		<ConfirmationModal
			confirmButtonClass="btn btn-warning"
			message="Your unsaved changes will be lost!"
			onCancel={ props.onCancel }
			onConfirm={ handleConfirmNavigation }
			onHide={ props.onCancel }
			shouldShow={ props.shouldShow }
			title="Are you sure?"
		/>
	);
}

NavigationConfirmationModal.propTypes = propTypes;
NavigationConfirmationModal.defaultProps = defaultProps;
export default withRouter(NavigationConfirmationModal);
