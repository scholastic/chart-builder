import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';

const propTypes = {
	cancelButtonClass: PropTypes.string,
	confirmButtonClass: PropTypes.string,
	message: PropTypes.string.isRequired,
	onCancel: PropTypes.func.isRequired,
	// `onConfirm(callback)`
	onConfirm: PropTypes.func.isRequired,
	onHide: PropTypes.func.isRequired,
	shouldShow: PropTypes.bool,
	title: PropTypes.string.isRequired,
}

const defaultProps = {
	cancelButtonClass: 'btn btn-outline-secondary',
	confirmButtonClass: 'btn btn-warning'
}

const ConfirmationModal = (props) => {

	return (
		<Modal show={ props.shouldShow } onHide={ props.onHide }>
			<Modal.Header>
				<Modal.Title>{ props.title }</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<p>{ props.message }</p>
			</Modal.Body>

			<Modal.Footer>
				<button
					className={ props.cancelButtonClass }
					onClick={ props.onCancel }
					type="button"
				>
					Cancel
				</button>

				<button
					type="button"
					className={ props.confirmButtonClass }
					onClick={ props.onConfirm }
				>
					Confirm
				</button>
			</Modal.Footer>
		</Modal>
	);
}

ConfirmationModal.propTypes = propTypes;
ConfirmationModal.defaultProps = defaultProps;
export default ConfirmationModal;
