import './Flash.styl';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

const propTypes = {
	duration: PropTypes.number,
	message: PropTypes.string,
	shouldShow: PropTypes.bool,
	type: PropTypes.oneOf(['success', 'error'])
};

const defaultProps = {
	duration: 1500,
	message: null,
	shouldShow: false,
	type: 'success'
}

const Flash = (props) => {
	return (
		<div className={ "flash flash-" + props.type + (props.shouldShow ? ' in' : ' out') }>
			<div className="flash-icon">
				<i className={ 'fas ' + (props.type === 'success' ? 'fa-check' : 'fa-exclamation-circle') } />
			</div>
			<div className="flash-message">
				{ props.message }
			</div>
		</div>
	);
};

Flash.propTypes = propTypes;
Flash.defaultProps = defaultProps;
export default Flash;
