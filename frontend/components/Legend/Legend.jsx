import './Legend.styl';
import PropTypes from 'prop-types';
import React from 'react';

const propTypes = {
	legendItems: PropTypes.arrayOf(PropTypes.shape({
		color: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired
	})),
};

const defaultProps = {
	legendItems: []
};

const Legend = (props) => {
	return (
		<div className="legend">
			{
				props.legendItems.map((legendItem, index) => {
					return (
						<div className="legend-item" key={ "legend-item-" + index }>
							<div className="legend-item-color" style={{ backgroundColor: legendItem.color }} />
							<div className="legend-item-label">
								{ legendItem.label }
							</div>
						</div>
					);
				})
			}
		</div>
	);
}

Legend.propTypes = propTypes;
Legend.defaultProps = defaultProps;
export default Legend;
