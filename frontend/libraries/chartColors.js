export default {
	red: 'rgb(198, 0, 29)',
	orange: 'rgb(241, 94, 0)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(86, 159, 1)',
	blue: 'rgb(0, 115, 176)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
}
