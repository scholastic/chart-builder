export default function fetchJson(url, fetchOptions = {}) {
	const defaultFetchOptions = {
		method: 'get',
		body: null,
		credentials: 'same-origin',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	};

	let modifiedFetchOptions = Object.assign(defaultFetchOptions, fetchOptions);

	if (fetchOptions.body) {
		modifiedFetchOptions.body = JSON.stringify(fetchOptions.body);
	}

	return fetch(url, modifiedFetchOptions)
	.then((response) => {
		if (response.status >= 200 && response.status < 300) {
			return Promise.resolve(response.json());
		} else {
			var error = new Error(response.statusText || response.status);
			error.response = response;
			return Promise.reject(error);
		}
	});
}
