<?php

return [
	'sentry' => [
		'frontend' => [
			'dsn' => env('SENTRY_FRONTEND_DSN')
		],
		'enabled' => env('SENTRY_ENABLED')
	]
];
