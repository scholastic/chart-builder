let path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');


module.exports = {
	mode: 'development',
	entry: './frontend/index.js',
	output: {
		path: path.resolve(__dirname, 'public/js/built'),
		// filename: 'bundle.[hash].js'
		filename: process.env.NODE_ENV === 'production' ? 'bundle.[hash].js' : 'bundle.js',
		publicPath: '/'
	},
	module: {
		rules: [
			{
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
			{ test: /\.css$/, use: ['style-loader', 'css-loader'] },
			{ test: /\.styl$/, use: ['style-loader', 'css-loader', 'stylus-loader'], exclude: /node_modules/ },
			{ test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
			{
				test: /\.less$/,
				use: [{
					loader: "style-loader" // creates style nodes from JS strings
				}, {
					loader: "css-loader" // translates CSS into CommonJS
				}, {
					loader: "less-loader" // compiles Less to CSS
				}]
			},
		]
	},
	resolve: {
		extensions: [".js", ".jsx", ".css", ".styl"]
	},
	plugins: [
		new WebpackNotifierPlugin({
			alwaysNotify: true
		}),

		new ProgressBarPlugin({ clear: false }),
	]
};
