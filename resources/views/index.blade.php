<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>LOI Chart Builder</title>
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

		@if (config('reporting.sentry.enabled'))
			<!-- sentry.io -->
			<script src="https://browser.sentry-cdn.com/4.6.3/bundle.min.js" crossorigin="anonymous"></script>
			<script>
				Sentry.init({
					dsn: '{{ config('reporting.sentry.frontend.dsn') }}',
					environment: '{{ config('sentry.environment') }}'
				});
			</script>
		@endif

		<!-- jQuery -->
		<script
			src="https://code.jquery.com/jquery-3.4.1.min.js"
			integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			crossorigin="anonymous"
		></script>

		<!-- jQuery mask -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

		<!-- FontAwesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

		<!-- Bootstrap CSS -->
		<link
			rel="stylesheet"
			href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
			integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
			crossorigin="anonymous"
		/>

		<!-- Google fonts -->
		<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
	</head>
	<body>
		<div id="app">
			<script src="/js/built/{{ $bundleFilename }}"></script>
		</div>
	</body>
</html>
