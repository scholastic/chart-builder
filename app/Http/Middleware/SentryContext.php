<?php

namespace App\Http\Middleware;

use Closure;
use Sentry\State\Scope;

class SentryContext {
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$student = session('student');

		if (app()->bound('sentry') && $student) {
			\Sentry\configureScope(function (Scope $scope) use ($student): void {
				if ($student) {
					$scope->setUser([
						'id' => $student->id,
						'username' => $student->username
					]);

					if ($student->school) {
						$scope->setTag('schoolId', $student->school->id);
						$scope->setTag('school', $student->school->name);

						if ($student->school->district) {
							$scope->setTag('districtId', $student->school->district->id);
							$scope->setTag('district', $student->school->district->name);
						}
					}
				}
			});
		}

		return $next($request);
	}
}
