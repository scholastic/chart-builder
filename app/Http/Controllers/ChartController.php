<?php

namespace App\Http\Controllers;

use App\Exceptions\Handler as ExceptionHandler;
use App\Models\Chart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Log;

class ChartController extends Controller {
	public function __construct(ExceptionHandler $handler) {
		$this->exceptionHandler = $handler;
	}

	public function show($uuid): Response {
		$chart = Chart::where(['uuid' => $uuid])->first();
		if ($chart) {
			return $this->_response(['success' => true, 'payload' => $chart->state]);
		} else {
			$response = ['success' => false, 'message' => 'Not found'];
			return $this->_response($response, 404);
		}
	}

	public function update($uuid, Request $request): Response {
		try {
			$chart = Chart::where(['uuid' => $uuid])->first();

			if ($chart) {
				$chart->state = $request->get('state');
				$chart->save();
				return $this->_response(['success' => true]);
			} else {
				return $this->store($request);
			}
		} catch (Exception $e) {
			$this->_logException($e);
			$message = "Failed to update chart: {$e->getMessage()}";
			$payload = ['trace' => $e->getTraceAsString()];
			$response = ['success' => false, 'message' => $message, 'payload' => $payload];
			return $this->_response($response, 500);
		}
	}

	public function store(Request $request): Response {
		try {
			$uuid = $request->get('uuid');
			$state = $request->get('state');

			if (!$uuid && !$state) {
				$response = ['success' => false, 'message' => 'Missing fields `uuid` and `state`'];
				return $this->_response($response, 400);
			}

			if (!$uuid) {
				$response = ['success' => false, 'message' => 'Missing field `uuid`'];
				return $this->_response($response, 400);
			}

			if (!$state) {
				$response = ['success' => false, 'message' => 'Missing field `state`'];
				return $this->_response($response, 400);
			}

			$saveData = [
				'uuid' => $uuid,
				'state' => $state
			];

			Chart::create($saveData);
			return $this->_response(['success' => true]);
		} catch (Exception $e) {
			$this->_logException($e);
			$message = "Failed to update chart: {$e->getMessage()}";
			$payload = ['trace' => $e->getTraceAsString()];
			$response = ['success' => false, 'message' => $message, 'payload' => $payload];
			return $this->_response($response, 500);
		}
	}

	/**
	 * Build and return the JSON response
	 * @param array|null $response Array to be serialized as JSON for response body
	 * @param int $httpCode HTTP status code
	 * @return Response
	 */
	protected function _response($response, int $httpCode = 200): Response {
		return response($response, $httpCode)->header('Content-Type', 'application/json');
	}

	/**
	 * Log an exception to the monolog
	 * @param Exception $e The exception to log
	 * @return void
	 */
	protected function _logException(Exception $e) {
		$parameters = json_encode(Input::all());
		$message = json_encode([
			'message' => $e->getMessage(),
			'file' => $e->getFile(),
			'line' => $e->getLine(),
			'request' => $parameters,
			'trace' => $e->getTrace()
		]);

		$this->exceptionHandler->report(new Exception($message));
	}
}
