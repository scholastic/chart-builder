<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;


class HealthController extends Controller {
	/**
	 * Health check action for AWS
	 *
	 * @param
	 *
	 * @return Response
	 */
	public function index() {
		return response('OK', 200)->header('Content-Type', 'text/plain');
	}

}
