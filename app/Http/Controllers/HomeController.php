<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller {
	public function index(Request $request) {
		$viewData = [
			'bundleFilename' => $this->_getBundleFilename()
		];

		return view('index', $viewData);
	}

	protected function _getBundleFilename() {
		// Our bundle filenames contain a content-based hash in production for cache busting.  See
		// `webpack.config.js` for details on the JS side of this.

		// Find the newest JS bundle
		$bundlePath = public_path('js/built/');
		$files = scandir($bundlePath, SCANDIR_SORT_DESCENDING);

		$unsortedBundles = array_filter($files, function($file) {
			return(preg_match('/^bundle.*\.js/', $file));
		});

		// Sort bundles by date modified descending
		uasort($unsortedBundles, function($a, $b) use ($bundlePath) {
			return filemtime("$bundlePath/$a") > filemtime("$bundlePath/$b") ? -1 : 1;
		});

		$sortedBundles = array_values($unsortedBundles);
		$newestBundle = $sortedBundles[0];
		return $newestBundle;
	}
}
